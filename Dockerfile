FROM httpd:2.4
 
ENV TZ America/New_York

COPY docker $HTTPD_PREFIX/conf/
COPY dist/demo-ui $HTTPD_PREFIX/htdocs

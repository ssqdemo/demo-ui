export interface Characters {
  content: Character[];
  totalElements: number;
}

export class Character {
  id: number;
  name: string;
  race: string;
  age: number;
}
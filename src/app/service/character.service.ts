import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Characters} from "../model/character.model";
import {Character} from "../model/character.model";
import { environment } from '../../environments/environment';

@Injectable()
export class CharacterService {


  constructor(private http: HttpClient) { }

  getCharacters(sort: string, order: string, page: number, itemsPerPage: number) {
    return this.http.get<Characters>(`${environment.apiBase}?page=${page}&size=${itemsPerPage}&sort=${sort},${order}`);
  }

  getCharacterById(id: number) {
    return this.http.get<Character>(environment.apiBase + '/' + id);
  }

  createCharacter(character: Character) {
    return this.http.post(environment.apiBase, character);
  }

  updateCharacter(character: Character) {
    return this.http.put(environment.apiBase + '/' + character.id, character);
  }

  deleteCharacter(id: number) {
    return this.http.delete(environment.apiBase + '/' + id);
  }
}

import { Component, AfterViewInit, ViewChild } from '@angular/core';
import {Router} from "@angular/router";
import {CharacterService} from "../service/character.service";
import {Character} from "../model/character.model";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs';
import {merge} from 'rxjs';
import {map} from 'rxjs/operators';
import {startWith} from 'rxjs/operators';
import {switchMap} from 'rxjs/operators';
import {catchError} from 'rxjs/operators';
import {of as observableOf} from 'rxjs';


@Component({
  selector: 'app-list-character',
  templateUrl: './list-character.component.html',
  styleUrls: ['./list-character.component.css']
})
export class ListCharacterComponent implements AfterViewInit {

  displayedColumns: string[] = ['id', 'name', 'race', 'age', 'actions'];
  dataSource = new MatTableDataSource();

  resultsLength = 0;
  isLoadingResults = false;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(private router: Router, private characterService: CharacterService) { }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit() {
    
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.characterService.getCharacters(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.totalElements;

          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          return observableOf([]);
        })
      ).subscribe(data => this.dataSource.data = data);
  }


  deleteCharacter(character: Character): void {
    this.characterService.deleteCharacter(character.id)
      .subscribe( data => {
        this.dataSource.data = this.dataSource.data.filter(u => u !== character);
      })
  };

  editCharacter(character: Character): void {
    localStorage.removeItem("editCharacterId");
    localStorage.setItem("editCharacterId", character.id.toString());
    this.router.navigate(['edit-character']);
  };

  addCharacter(): void {
    this.router.navigate(['add-character']);
  };


}




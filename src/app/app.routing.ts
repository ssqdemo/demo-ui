import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {AddCharacterComponent} from "./add-character/add-character.component";
import {ListCharacterComponent} from "./list-character/list-character.component";
import {EditCharacterComponent} from "./edit-character/edit-character.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'add-character', component: AddCharacterComponent },
  { path: 'list-character', component: ListCharacterComponent },
  { path: 'edit-character', component: EditCharacterComponent },
  { path: '', component : LoginComponent}
];

export const routing = RouterModule.forRoot(routes);

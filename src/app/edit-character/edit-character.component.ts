import { Component, OnInit } from '@angular/core';
import {CharacterService} from "../service/character.service";
import {Router} from "@angular/router";
import {Character} from "../model/character.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-edit-character',
  templateUrl: './edit-character.component.html',
  styleUrls: ['./edit-character.component.css']
})
export class EditCharacterComponent implements OnInit {

  character: Character;
  editForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private characterService: CharacterService) { }

  ngOnInit() {
    let characterId = localStorage.getItem("editCharacterId");
    if(!characterId) {
      alert("Invalid action.")
      this.router.navigate(['list-character']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [],
      name: ['', Validators.required],
      race: ['', Validators.required],
      age: ['', Validators.required]
    });
    this.characterService.getCharacterById(+characterId)
      .subscribe( data => {
        this.editForm.setValue(data);
      });
  }

  onSubmit() {
    this.characterService.updateCharacter(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['list-character']);
        },
        error => {
          alert(error);
        });
  }

}

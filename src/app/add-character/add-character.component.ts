import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CharacterService} from "../service/character.service";
import {first} from "rxjs/operators";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-Character',
  templateUrl: './add-character.component.html',
  styleUrls: ['./add-character.component.css']
})
export class AddCharacterComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router, private characterService: CharacterService) { }

  addForm: FormGroup;

  ngOnInit() {

    this.addForm = this.formBuilder.group({
      id: [],
      name: ['', Validators.required],
      race: ['', Validators.required],
      age: ['', Validators.required]
    });

  }

  onSubmit() {
    this.characterService.createCharacter(this.addForm.value)
      .subscribe( data => {
        this.router.navigate(['list-character']);
      });
  }

}
